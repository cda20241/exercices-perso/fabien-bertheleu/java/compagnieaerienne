package org.example;
import java.lang.String;
public class Billet {
    private String numBillet;
    private String dateEmission;
    private String dateReservation;
    private String datePaiement;
    private Passager passager;
    private Siege siege;

    private Vol vol;



    //     CONSTRUCTEUR    //
    public Billet(String numBillet, String dateEmission, String dateReservation, String datePaiement) {
        this.numBillet = numBillet;
        this.dateEmission = dateEmission;
        this.dateReservation = dateReservation;
        this.datePaiement = datePaiement;
    }

    public Billet(String numBillet, Passager passager, Siege siege) {
        this.numBillet = numBillet;
        this.passager = passager;
        this.siege = siege;
    }

    //     GETTER    //
    public String getNumBillet() {
        return numBillet;
    }

    public String getDateEmission() {
        return dateEmission;
    }

    public String getDateReservation() {
        return dateReservation;
    }

    public String getDatePaiement() {
        return datePaiement;
    }

    public Passager getPassager() {
        return passager;
    }

    public Siege getSiege() {
        return siege;
    }

    //     SETTER     //

    public void setNumBillet(String numBillet) {
        this.numBillet = numBillet;
    }

    public void setDateEmission(String dateEmission) {
        this.dateEmission = dateEmission;
    }

    public void setDateReservation(String dateReservation) {
        this.dateReservation = dateReservation;
    }

    public void setDatePaiement(String datePaiement) {
        this.datePaiement = datePaiement;
    }

    public void setPassager(Passager passager) {
        this.passager = passager;
    }

    public void setSiege(Siege siege) {
        this.siege = siege;
    }

    //     METHODES    //

    public void impressionInfosBillet() {
        System.out.println("Passager : " + Passager.nomPassager());
        System.out.println("Numero Billet : " + numBillet);
        System.out.println("Date Emissision du Billet : " + dateEmission);
        System.out.println("Date de Reservation : " + dateReservation);
    }

//    public void infosDuSiege(numBillet, Siege){
//        this.numBillet= p_Siege;
//        this.setBilletduSiege(this);

}