package org.example;

public class Pilote {

    String matriculePilote;
    String nomPilote;
    String prenomPilote;
    String qualifPilote;
    Compagnie compagnie;
    Vol vol;

    String employeur = new String();

    //      CONSTRUCTEUR    //
    public Pilote(String matriculePilote, String nomPilote, String prenomPilote, String qualifPilote) {
        this.matriculePilote = matriculePilote;
        this.nomPilote = nomPilote;
        this.prenomPilote = prenomPilote;
        this.qualifPilote = qualifPilote;
    }
    //      GETTER      //
    public String getMatriculePilote() {
        return matriculePilote;
    }

    public String getNomPilote() {
        return nomPilote;
    }

    public String getPrenomPilote() {
        return prenomPilote;
    }

    public String getQualifPilote() {
        return qualifPilote;
    }

    //      SETTER      //
    public void setMatriculePilote(String matriculePilote) {
        this.matriculePilote = matriculePilote;
    }

    public void setNomPilote(String nomPilote) {
        this.nomPilote = nomPilote;
    }

    public void setPrenomPilote(String prenomPilote) {
        this.prenomPilote = prenomPilote;
    }

    public void setQualifPilote(String qualifPilote) {
        this.qualifPilote = qualifPilote;
    }

    //      METHODES        //




    public void employeParCompagnie(Compagnie employeur){
        this.compagnie = employeur;
        this.compagnie.setPilote(this);
    }
}
