package org.example;

import java.util.ArrayList;
import java.util.List;

public class Compagnie {

    public String codeCompagnie;
    public String nomCompagnie;
    public String siegeSocial;
    public Pilote pilote;
    public Vol vol;

    List<Pilote>pilotes = new ArrayList();


    //      CONSTRUCTEUR        //
    public Compagnie(String codeCompagnie, String nomCompagnie, String siegeSocial) {
        this.codeCompagnie = codeCompagnie;
        this.nomCompagnie = nomCompagnie;
        this.siegeSocial = siegeSocial;
    }
    //      GETTER      //
    public String getCodeCompagnie() {
        return codeCompagnie;
    }

    public String getNomCompagnie() {
        return nomCompagnie;
    }

    public String getSiegeSocial() {
        return siegeSocial;
    }

    //      SETTER      //
    public void setCodeCompagnie(String codeCompagnie) {
        this.codeCompagnie = codeCompagnie;
    }

    public void setNomCompagnie(String nomCompagnie) {
        this.nomCompagnie = nomCompagnie;
    }

    public void setSiegeSocial(String siegeSocial) {
        this.siegeSocial = siegeSocial;
    }
    public void setPilote(Pilote pilote) {
        this.pilote = pilote;
    }

    public void setVol(Vol vol) {
        this.vol = vol;
    }

    public void setPilotes(List<Pilote> pilotes) {
        this.pilotes = pilotes;
    }


    //      METHODES        //

    public void embauchePilote(Pilote pilote) {
        this.pilotes.add(pilote);
        System.out.println(nomCompagnie + " a embauché " + this);
    }
    public void vireLePilote(Pilote pilote) {
        this.pilotes.remove(this);
        System.out.println(this + " ne travaille plus pour " +nomCompagnie)  ;
    }
}

