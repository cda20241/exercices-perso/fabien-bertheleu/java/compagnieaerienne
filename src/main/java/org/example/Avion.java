package org.example;

public class Avion {
    String codeAvion;
    String typeAvion;
    String modeleAvion;
    String nombrePassagers;


    //      CONSTRUCTEURS       //
    public Avion(String codeAvion, String typeAvion, String modeleAvion, String nombrePassagers) {
        this.codeAvion = codeAvion;
        this.typeAvion = typeAvion;
        this.modeleAvion = modeleAvion;
        this.nombrePassagers = nombrePassagers;
    }
    //      SETTER      //
    public String getCodeAvion() {
        return codeAvion;
    }

    public String getTypeAvion() {
        return typeAvion;
    }

    public String getModeleAvion() {
        return modeleAvion;
    }

    public String getNombrePassagers() {
        return nombrePassagers;
    }

    public void setCodeAvion(String codeAvion) {
        this.codeAvion = codeAvion;
    }

    public void setTypeAvion(String typeAvion) {
        this.typeAvion = typeAvion;
    }

    public void setModeleAvion(String modeleAvion) {
        this.modeleAvion = modeleAvion;
    }

    public void setNombrePassagers(String nombrePassagers) {
        this.nombrePassagers = nombrePassagers;
    }
}
