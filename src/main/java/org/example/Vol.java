package org.example;
import java.lang.String;

public class Vol {

    public String numVol;
    private String dateDepart;
    public String dateArrivee;
    public String heureDepart;
    public String heureArrivee;
    public String villeDepart;
    public String villeArrivee;
    public String retard;
    public Billet numeroBillet;
    public String nomPassager;


    public Vol(String numVol, Billet numeroBillet) {
        this.numVol = numVol;
        this.numeroBillet = numeroBillet;
    }

    //      CONSTRUCTEURS       //
    public Vol(String numVol, String dateDepart, String dateArrivee, String heureDepart, String heureArrivee, String villeDepart, String villeArrivee, String retard) {
        this.numVol = numVol;
        this.dateDepart = dateDepart;
        this.dateArrivee = dateArrivee;
        this.heureDepart = heureDepart;
        this.heureArrivee = heureArrivee;
        this.villeDepart = villeDepart;
        this.villeArrivee = villeArrivee;
        this.retard = retard;



    }


    //      GETTER      //
    public void setNumVol(String numVol) {
        this.numVol = numVol;
    }

    public void setDateDepart(String dateDepart) {
        this.dateDepart = dateDepart;
    }

    public void setDateArrivee(String dateArrivee) {
        this.dateArrivee = dateArrivee;
    }

    public void setHeureDepart(String heureDepart) {
        this.heureDepart = heureDepart;
    }

    public void setHeureArrivee(String heureArrivee) {
        this.heureArrivee = heureArrivee;
    }

    public void setVilleDepart(String villeDepart) {
        this.villeDepart = villeDepart;
    }

    public void setVilleArrivee(String villeArrivee) {
        this.villeArrivee = villeArrivee;
    }

    public void setRetard(String retard) {
        this.retard = retard;
    }

    public Billet getNumeroBillet() {
        return numeroBillet;
    }

    public String getNomPassager() {
        return nomPassager;
    }

    //      SETTER      //
    public String getNumVol() {
        return numVol;
    }

    public String getDateDepart() {
        return dateDepart;
    }

    public String getDateArrivee() {
        return dateArrivee;
    }

    public String getHeureDepart() {
        return heureDepart;
    }

    public String getHeureArrivee() {
        return heureArrivee;
    }

    public String getVilleDepart() {
        return villeDepart;
    }

    public String getVilleArrivee() {
        return villeArrivee;
    }

    public String getRetard() {
        return retard;
    }

    public void setNumeroBillet(Billet numeroBillet) {
        this.numeroBillet = numeroBillet;
    }

    public void setNomPassager(String nomPassager) {
        this.nomPassager = nomPassager;
    }

    //      METHODES    //







}