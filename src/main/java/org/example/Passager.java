package org.example;

public class Passager {

    public String nomPassager;
    public String prenomPassager;
    public String dateNaissance;
    public String telephone;
    public String Statut;

    public Billet billetDuVol;


    //      CONSTRUCTEURS       //
    public Passager(String nomPassager, String prenomPassager, String dateNaissance, String telephone, String statut) {
        this.nomPassager = nomPassager;
        this.prenomPassager = prenomPassager;
        this.dateNaissance = dateNaissance;
        this.telephone = telephone;
        Statut = statut;
    }

    public Passager(String nomPassager, Billet billetDuVol) {
        this.nomPassager = nomPassager;
        this.billetDuVol = billetDuVol;
    }

    //      GETTER      //
    public String getNomPassager() {
        return nomPassager;
    }

    public String getPrenomPassager() {
        return prenomPassager;
    }

    public String getDateNaissance() {
        return dateNaissance;
    }

    public String getTelephone() {
        return telephone;
    }

    public String getStatut() {
        return Statut;
    }

    public Billet getBilletDuVol() {
        return billetDuVol;
    }

    //      SETTER      //

    public void setNomPassager(String nomPassager) {
        this.nomPassager = nomPassager;
    }

    public void setPrenomPassager(String prenomPassager) {
        this.prenomPassager = prenomPassager;
    }

    public void setDateNaissance(String dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public void setTelephone(String telephone) { this.telephone = telephone; }
    public void setStatut(String statut) {
        Statut = statut;
    }

    public void setBilletDuVol(Billet billetDuVol) {
        this.billetDuVol = billetDuVol;
    }

    //      METHODES        //
    public void prendreBillet(Billet p_billet){
        this.setBilletDuVol(p_billet);
    }


}