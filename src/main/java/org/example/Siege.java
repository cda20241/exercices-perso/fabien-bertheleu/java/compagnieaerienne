package org.example;

public class Siege {
    Integer numAllee;
    Integer numRang;
    String classeSiege;
    public String BilletDuSiege;
    public Siege(Integer numAllee, Integer numRang, String classeSiege) {
        this.numAllee = numAllee;
        this.numRang = numRang;
        this.classeSiege = classeSiege;
            }
    //      GETTER      //
    public Integer getNumAllee() {
        return numAllee;
    }

    public Integer getNumRang() {
        return numRang;
    }

    public String getBilletDuSiege() {
        return BilletDuSiege;
    }


    //      SETTER      //
    public String getClasseSiege() {
        return classeSiege;
    }

    public void setNumAllee(Integer numAllee) {
        this.numAllee = numAllee;
    }

    public void setNumRang(Integer numRang) {
        this.numRang = numRang;
    }

    public void setClasseSiege(String classeSiege) {
        this.classeSiege = classeSiege;
    }

    public void setBilletDuSiege(String billetDuSiege) {
        BilletDuSiege = billetDuSiege;
    }
}
